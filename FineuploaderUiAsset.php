<?php

namespace azbuco\fineuploader;

class FineuploaderUiAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/fine-uploader';

    public $css = [
        'dist/fine-uploader.css',
        'dist/fine-uploader-new.min.css',
        'dist/fine-uploader-gallery.min.css',
    ];
    public $depends = [
        'azbuco\fineuploader\FineuploaderBasicAsset',
    ];
}
