[Fineuploader](http://fineuploader.com/) widget for Yii2 applications

There are two widgets in this package

* FineuploaderBasic: for the fileuploader api only
* FineuploaderUi: for the fileuploader api and user interface

## FineuploaderBasic ##


```
#!php
<?=
azbuco\fineuploader\FineuploaderBasic::widget([
    // id tag for the widget, optional
    'id' => 'fineuploader',

    // the widget tag, optional
    'tag' => 'span',

    // the widget html content, required 
    // a click on this will trigger the upload dialog
    'text' => 'Excel import (xlsx)',

    // url for the upload handler
    'endpoint' => \yii\helpers\Url::to(['...']),

    // extra params send with the upload
    'params' => [
        Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
    ],

    // file dialog restricted to this file types
    'acceptFiles' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',

    // file dialog restricted to this extension
    'allowedExtensions' => ['xlsx'],

    // allow multiple file selection in the dialog
    'multiple' => false,

    // fineuploader events, see the fineuploader api
    'clientEvents' => [
        'onUpload' => new JsExpression('function(id, name) { /* do what you want */ }'),
        'onComplete' => new JsExpression('function(id, name, data, xhr) { /* do what you want */ }'),
        'onError' => new JsExpression('function(id, name, error, xhr) { /* do what you want */ }'),
    ],
])
?>
```

## FineuploaderUi ##

```
#!php
<?= FineuploaderUi::widget([
    // id tag for the widget, optional
    'id' => 'fineuploader',

    // template file (@azbuco/fineuploader/views/default)
    'template' => '//layouts/...',

    // url for the upload handler
    'endpoint' => Url::to(['...']),
    
    // extra params send with the upload
    'params' => [
        Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
    ],

    // file dialog restricted to this file types
    'acceptFiles' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',

    // file dialog restricted to this extension
    'allowedExtensions' => ['xlsx'],

    // allow multiple file selection in the dialog
    'multiple' => false,

    // fineuploader client options, see the fineuploader api
    'clientOptions' => [
        'classes' => [
            'success' => 'alert alert-success hidden',
            'fail' => 'alert alert-error'
        ],
    ],

    // fineuploader events, see the fineuploader api 
    'clientEvents' => [
        'onUpload' => new JsExpression('function(id, name) { /* do what you want */ }'),
        'onComplete' => new JsExpression('function(id, name, data, xhr) { /* do what you want */ }'),
        'onError' => new JsExpression('function(id, name, error, xhr) { /* do what you want */ }'),
    ]
]);
?>
```