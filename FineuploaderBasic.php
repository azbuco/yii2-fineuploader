<?php

namespace azbuco\fineuploader;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;

/**
 * Class FineuploaderBasic
 * Use this widget when you only need a button or link for the upload without the UI
 * @package azbuco\fineuploader
 */
class FineuploaderBasic extends FineuploaderCore
{

    /**
     * @var string The tag to use as the 'select files' button. 
     * It can not be a 'button' tag
     * @see http://docs.fineuploader.com/branch/master/api/options.html#button
     */
    public $tag = 'a';

    /**
     * @var string Text for the upload button
     */
    public $text = 'Upload';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->clientOptions['uploaderType'] = 'basic';
        $this->clientOptions['button'] = new JsExpression('$("#' . $this->getId() . '")[0]');
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerBundle();
        $this->registerClientScript();

        return Html::tag($this->tag, $this->text, $this->options);
    }

    /**
     * Registers plugin and the related events
     */
    protected function registerBundle()
    {
        $view = $this->getView();
        FineuploaderBasicAsset::register($view);
    }

    /**
     * Register JS
     */
    protected function registerClientScript()
    {
        $id = $this->getId();
        $clientOptions = $this->clientOptions;
        $clientOptions['callbacks'] = [];
        foreach ($this->clientEvents as $name => $event) {
            $clientOptions['callbacks'][$name] = $event;
        }

        $js = ';$("#' . $id . '").fineUploader(' . Json::encode($clientOptions) . ')';
        $this->getView()->registerJs($js);
    }

}
