<?php

namespace azbuco\fineuploader;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

/**
 * Class FineuploaderUi
 * Use this widget when you need a full upload UI.
 * @package azbuco\fineuploader
 */
class FineuploaderUi extends FineuploaderCore
{
    /**
     * @var string Path to template view. The template must follow the fineuploader styling conventions
     * @see http://docs.fineuploader.com/branch/master/features/styling.html
     */
    public $template = '@azbuco/fineuploader/views/default';

    /**
     * @var [] localized fineupload messages
     * @see http://docs.fineuploader.com/branch/master/api/options.html#messages
     */
    protected $_defaultMessages;

    /**
     * @var [] localized fineupload texts
     * @see http://docs.fineuploader.com/branch/master/api/options.html#text
     */
    protected $_defaultTexts;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->registerTranslations();

        // init localization
        $this->_defaultMessages = [
            'emptyError' => FineuploaderUi::t('message', '{file} is empty, please select files again without it.'),
            'maxHeightImageError' => FineuploaderUi::t('message', 'Image is too tall.'),
            'maxWidthImageError' => FineuploaderUi::t('message', 'Image is too wide.'),
            'minHeightImageError' => FineuploaderUi::t('message', 'Image is not tall enough.'),
            'minWidthImageError' => FineuploaderUi::t('message', 'Image is not wide enough.'),
            'minSizeError' => FineuploaderUi::t('message', '{file} is too small, minimum file size is {minSizeLimit}.'),
            'noFilesError' => FineuploaderUi::t('message', 'No files to upload.'),
            'onLeave' => FineuploaderUi::t('message', 'The files are being uploaded, if you leave now the upload will be canceled.'),
            'retryFailTooManyItemsError' => FineuploaderUi::t('message', 'Retry failed - you have reached your file limit.'),
            'sizeError' => FineuploaderUi::t('message', '{file} is too large, maximum file size is {sizeLimit}.'),
            'tooManyItemsError' => FineuploaderUi::t('message', 'Too many items ({netItems}) would be uploaded. Item limit is {itemLimit}.'),
            'typeError' => FineuploaderUi::t('message', '{file} has an invalid extension. Valid extension(s): {extensions}.'),
            'unsupportedBrowserIos8Safari' => FineuploaderUi::t('message', 'Unrecoverable error - this browser does not permit file uploading of any kind due to serious bugs in iOS8 Safari. Please use iOS8 Chrome until Apple fixes these issues.'),
        ];

        $this->_defaultTexts = [
            // core texts
            'defaultResponseError' => FineuploaderUi::t('message', 'Upload failure reason unknown.'),
            'fileInputTitle' => FineuploaderUi::t('message', 'file input'),
            // ui texts
            'failUpload' => FineuploaderUi::t('message', 'Upload failed'),
            'formatProgress' => FineuploaderUi::t('message', '{percent}% of {total_size}'),
            'paused' => FineuploaderUi::t('message', 'Paused'),
            'waitingForResponse' => FineuploaderUi::t('message', 'Processing...'),
        ];

        $this->clientOptions['template'] = ltrim(preg_replace("/[^A-Za-z0-9]/", '-', $this->template), '-');
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerBundle();
        $this->registerClientScript();

        if (!isset(Yii::$app->params['@azbuco/fineuploader/templates'])) {
            Yii::$app->params['@azbuco/fineuploader/templates'] = [];
        }

        if (!isset(Yii::$app->params['@azbuco/fineuploader/templates'][$this->template])) {
            $template = $this->getView()->render($this->template, [
                'widget' => $this,
            ]);

            // wrap template to script tag
            $template = Html::tag('script', $template, [
                    'id' => $this->clientOptions['template'],
                    'type' => 'text/template',
            ]);

            Yii::$app->params['@azbuco/fineuploader/templates'][$this->template] = $template;
        }

        $this->view->on(View::EVENT_BEGIN_BODY, function() {
            echo Yii::$app->params['@azbuco/fineuploader/templates'][$this->template];
        });

        $options = Json::encode($this->options);

        return '<div id="' . $this->getId() . '"></div>';
    }

    /**
     * Registers plugin and the related events
     */
    protected function registerBundle()
    {
        $view = $this->getView();
        FineuploaderUiAsset::register($view);
    }

    /**
     * Register JS
     */
    protected function registerClientScript()
    {
        $id = $this->getId();
        $clientOptions = Json::encode($this->clientOptions);
        
        $js = ';$("#' . $id . '").fineUploader(' . $clientOptions . ')';
        foreach($this->clientEvents as $name => $event) {
            $js .= '.on("' . $name . '", ' . $event . ')';
        }
        $js .= ';';

        $this->getView()->registerJs($js);
    }

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['azbuco/fineuploader/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@azbuco/fineuploader/messages',
            'fileMap' => [
                'azbuco/fineuploader/message' => 'message.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('azbuco/fineuploader/' . $category, $message, $params, $language);
    }
}
