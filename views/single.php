<?php

use azbuco\fineuploader\FineuploaderUi;

/* @var $this View */
/* @var $widget FineuploaderUi */
?>

<div class="qq-uploader-selector">

    <div class="qq-upload-button-selector btn btn-primary">
        <div><?= FineuploaderUi::t('message', 'Upload a file') ?></div>
    </div>

    <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
        <li>
            <div class="progress qq-progress-bar-container-selector">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-success qq-progress-bar-selector"></div>
            </div>
            <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
            <span class="qq-upload-file-selector qq-upload-file"></span>
            <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
            <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
            <span class="qq-upload-size-selector qq-upload-size"></span>
            <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel"><?= FineuploaderUi::t('message', 'Cancel') ?></button>
            <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry"><?= FineuploaderUi::t('message', 'Retry') ?></button>
            <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete"><?= FineuploaderUi::t('message', 'Delete') ?></button>
            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
        </li>
    </ul>

    <dialog class="qq-alert-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector"><?= FineuploaderUi::t('message', 'Close') ?></button>
        </div>
    </dialog>

    <dialog class="qq-confirm-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector"><?= FineuploaderUi::t('message', 'No') ?></button>
            <button type="button" class="qq-ok-button-selector"><?= FineuploaderUi::t('message', 'Yes') ?></button>
        </div>
    </dialog>

    <dialog class="qq-prompt-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <input type="text">
        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector"><?= FineuploaderUi::t('message', 'Cancel') ?></button>
            <button type="button" class="qq-ok-button-selector"><?= FineuploaderUi::t('message', 'Ok') ?></button>
        </div>
    </dialog>
</div>
