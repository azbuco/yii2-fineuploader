<?php

namespace azbuco\fineuploader;

use yii\base\Widget;
use yii\web\JsExpression;

/**
 * Class FineuploaderCore
 * This is the parent class for FineuploaderBasic and FineuploaderUi
 * Do not use the file directly.
 * @package azbuco\fineuploader
 */
class FineuploaderCore extends Widget
{
    /**
     * @var [] Wrapper HTML options
     */
    public $options;

    /**
     * @var string URL for the upload request.
     */
    public $endpoint;

    /**
     * @var [] Extra params for the upload request (for more configuration options use the clientOptions['request'] property)
     */
    public $params = [];

    /**
     * @var boolean Allow multiple file upload
     */
    public $multiple = true;

    /**
     * @var string Comma-delimited list of valid MIMEtypes (used by the selection dialog)
     * @see https://docs.fineuploader.com/branch/master/api/options.html#validation
     */
    public $acceptFiles = 'image/jpeg, image/png, image/gif';

    /**
     * @var [] Valid file extension to restrict upload file types
     * @see https://docs.fineuploader.com/branch/master/api/options.html#validation
     */
    public $allowedExtensions = ['jpeg', 'jpg', 'png', 'gif'];
    
    /**
     * @var string upload file input name
     * @see https://docs.fineuploader.com/branch/master/api/options.html#request
     * 
     * It should be something like Model[uploadFile] if you use model for upload.
     * @see https://www.yiiframework.com/doc/guide/2.0/en/input-file-upload
     */
    public $inputName = 'qqfile';

    /**
     * @var [] Extra fineuploader options
     * @see https://docs.fineuploader.com/branch/master/
     */
    public $clientOptions = [];

    /**
     * @var [] Fineuploader events
     * @see https://docs.fineuploader.com/branch/master/
     */
    public $clientEvents = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // setup options
        $this->options['id'] = $this->getId();

        // setup clientOptions
        if (!isset($this->clientOptions['request'])) {
            $this->clientOptions['request'] = [];
        }
        if (!isset($this->clientOptions['validation'])) {
            $this->clientOptions['validation'] = [];
        }
        $this->clientOptions['request']['endpoint'] = $this->endpoint;
        $this->clientOptions['request']['params'] = $this->params;
        $this->clientOptions['request']['inputName'] = $this->inputName;
        $this->clientOptions['multiple'] = $this->multiple;
        $this->clientOptions['validation']['acceptFiles'] = $this->acceptFiles;
        $this->clientOptions['validation']['allowedExtensions'] = $this->allowedExtensions;
    }
}
