<?php

namespace azbuco\fineuploader;

class FineuploaderBasicAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/fine-uploader';
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public function init()
    {
        $this->js[] = YII_DEBUG ? 'dist/jquery.fine-uploader.js' : 'dist/jquery.fine-uploader.min.js';

        parent::init();
    }
}
